#ifndef ENTRYLIST_H
#define ENTRYLIST_H

#include <QList>

#include <QAbstractListModel>
#include <QStringList>

class Entry;

class EntryList : public QAbstractListModel
{
    Q_OBJECT

    //Q_PROPERTY(QList <Entry*> list READ list WRITE createElement NOTIFY listChanged)
public:

    enum Roles {
       ColorRole = Qt::UserRole + 1,
       HeaderRole
    };

    EntryList(QObject *parent = 0);

    //QList <Entry*> list() { return m_entry; }

    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QString &value, int role);
    Qt::ItemFlags flags(const QModelIndex &index) const;

    Q_INVOKABLE void add(); //

    Q_INVOKABLE void createElement();
    void createElement(QString tmpHeader, QString tmpDate, QString tmpTime, QString tmpText);

    void loadList();
    void createList();

protected:
    QHash<int, QByteArray> roleNames() const;

private:

    QList <Entry*> m_entry;

signals:
    void listChanged(QList <Entry*> m_entry);
};
#endif // ENTRYLIST_H
