//#include <QtQml>
#include <QApplication>

#include <QQmlApplicationEngine>
#include <qqmlcontext.h>
#include "entrylist.h"
#include "entry.h"
#include "pageday.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    qmlRegisterType<EntryList>("ListLogic", 1, 0, "ListLogic");
    qmlRegisterType<Entry>("Entry", 1, 0, "Entry");
    qmlRegisterType<PageDay>("DayLogic", 1, 0, "DayLogic");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    EntryList *list= new EntryList();

    //QQmlContext *ctxt = engine.rootContext();
    engine.rootContext()->setContextProperty("listModel", list);
    //ctxt->setContextProperty("listModel", list);

    return app.exec();
}
