#ifndef ENTRY_H
#define ENTRY_H

#include <QObject>
//#include <QUuid>
//#include "pageday.h"

QT_BEGIN_NAMESPACE
//class PageDay;
QT_END_NAMESPACE

class Entry : public QObject
{

Q_OBJECT

    //Q_PROPERTY(QString color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(QString header READ header WRITE setHeader NOTIFY headerChanged)
    //Q_PROPERTY(QString id READ id WRITE setId NOTIFY IdChanged) //
    Q_PROPERTY(QString date READ date WRITE setDate NOTIFY dateChanged)
    Q_PROPERTY(QString time READ time WRITE setTime NOTIFY timeChanged)
    Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)

private:
    QString m_header;
    //QUuid   m_id; //
    QString m_date;
    QString m_time;
    QString m_text;

public:
    explicit Entry(QObject* pobj = 0 );

    //void generateId();

    //Q_INVOKABLE void setId(QString &str);
    //QUuid getId() const { return m_id; }
    void setHeader(QString str);
    QString header() { return m_header; }
    void setDate(QString str);
    QString date() const { return m_date; }
    void setTime(QString str);
    QString time() const { return m_time; }
    void setText(QString str);
    QString text() const { return m_text; }

signals:
    //void colorChanged(QString color);
    void headerChanged();
    void dateChanged();
    void timeChanged();
    void textChanged();
};

#endif // ENTRY_H
