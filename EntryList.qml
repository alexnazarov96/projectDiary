import QtQuick 2.0
import ListLogic 1.0

Rectangle {

    color: "#decd78"

    FontLoader {
        id: customFont
        name: "PlaneWalker"
        source: "qrc:///planewalker.ttf"
    }

    //ListLogic {
//        id: listLogic
//    }

    ListView {
        id: listView
        //x: 15
        //y: 14
        width: 611
        height: 450
        boundsBehavior: Flickable.DragAndOvershootBounds
        highlightRangeMode: ListView.NoHighlightRange
        spacing: 10

        model: listModel
        delegate: Text {
            Text {
                //lineHeightMode: Text.ProportionalHeight(2)
                font.pixelSize: 20
                font.family: customFont.name
                text: header
            }
        }

    }
}
