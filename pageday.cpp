#include <QDateTime>
#include <QDebug>
#include "pageday.h"

PageDay::PageDay(QObject* pobj) : QObject(pobj)
{
    initDate();
    initTime();
}

void PageDay::setDate(QString str)
{
    date = str;
}
void PageDay::setTime(QString str)
{
    time = str;
}
void PageDay::setText(QString str)
{
    text = str;
}

void PageDay::initDate()
{
    QDateTime tmp = QDateTime::currentDateTime();
    date = tmp.date().toString("dd MMMM yyyy");
    //qDebug()<<date;

}

void PageDay::initTime()
{
    QDateTime tmp = QDateTime::currentDateTime();
    time = tmp.time().toString("hh:mm");
    //qDebug()<<time;
}
