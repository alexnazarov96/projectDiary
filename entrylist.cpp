#include <QCoreApplication>
#include <QDir>
#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QMessageBox>
#include <QDebug>

#include "entrylist.h"
#include "entry.h"

EntryList::EntryList(QObject *parent)
    : QAbstractListModel(parent)
{
    loadList();
}


void EntryList::createElement()
{
    Entry *entry = new Entry(this);

    entry->setHeader("...");
    //saveElement(); // save entry to xml file
    //emit listChanged();
    m_entry << entry; // mp append()
    //emit dataChanged();
}

void EntryList::createElement(QString tmpHeader, QString tmpDate, QString tmpTime, QString tmpText )
{
    Entry *entry = new Entry(this);
    qDebug()<< "createElement(args): created new entry";
    //  fill check?
    //if(!tmpHeader.isEmpty())
    entry->setHeader(tmpHeader);
    //entry->id = tmpQuuid;
    entry->setDate(tmpDate);
    entry->setTime(tmpTime);
    entry->setText(tmpText);
    qDebug()<< "createElement(args): variables set";

    m_entry << entry;
    //setHeaders(); //
    qDebug()<< "createElement(args): done";
    //emit dataChanged();
}


//

int EntryList::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }

    return m_entry.size();
}

QVariant EntryList::data(const QModelIndex &index, int role) const
{
    qDebug()<<" Data asked for "<<index.row()<<" and role "<<role;

    if (!index.isValid()) {
        return QVariant();
    }

    //m_entry.at(index.row());
    Entry* entry = m_entry[index.row()];

    switch (role) {
    case ColorRole:
        return QVariant(index.row() < 2 ? "orange" : "skyblue"); //
    case HeaderRole:
        qDebug()<<"EntryList::data";
        return QVariant::fromValue(entry->header());
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> EntryList::roleNames() const
{
    QHash<int, QByteArray> roles = QAbstractListModel::roleNames();
    roles[ColorRole] = "color";
    roles[HeaderRole] = "header";

    return roles;
}

void EntryList::add()
{
    beginInsertRows(QModelIndex(), m_entry.size(), m_entry.size());
    //m_entry.append("...");
    endInsertRows();

    //m_entry[0] = QString("Size: %1").arg(m_entry.size());
    QModelIndex index = createIndex(0, 0, static_cast<void *>(0));
    emit dataChanged(index, index);
}


bool EntryList::setData(const QModelIndex &index, const QString &value, int role)
{
    if (!index.isValid()) {
        return false;
    }

    Entry* entry = m_entry.at(index.row());

    switch (role) {
    case ColorRole:
        return false;   // This property can not be set
    case HeaderRole:
        entry->setHeader(value);
        break;
    default:
        return false;
    }

    emit dataChanged(index, index, QVector<int>() << role);

    return true;
}

Qt::ItemFlags EntryList::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractListModel::flags(index) | Qt::ItemIsEditable;
}



//
/*
void EntryList::changeElement(Entry entry)
{
    emit listChanged();
}

void EntryList::deleteElement(Entry entry)
{
    emit listChanged();
}

void EntryList::saveElement(Entry entry)
{
    // saves elements to xml file
    emit listChanged();
}
*/

void EntryList::loadList()
{
    QDir dir(QCoreApplication::applicationDirPath() + "/profiles");
    if (!dir.exists()) {
        dir.mkpath(".");

        QMessageBox msgBox;
        msgBox.setText("No profile dir found, created new");
        msgBox.exec();

        createList();   // test if this shit needed here
        //return;
    }

    QFile file(QCoreApplication::applicationDirPath() + "/profiles/user.xml"); //
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {

        QMessageBox msgBox;
        msgBox.setText("Error: file not found, created new profile");
        msgBox.exec();
        //return;
        createList();  // test if this shit needed here
    }

    qDebug()<< "file opened successfuly";

    if(file.size() == 0) // check if file is empty
    {
       qDebug()<< "No data to read from file";
       return;
    }

    QString tmpHeader, tmpText, tmpDate, tmpTime; // tmp vars to read data

    QXmlStreamReader xmlReader(&file);

    if (xmlReader.readNextStartElement())
    {
        if (xmlReader.name() == "days")
        {
            while(xmlReader.readNextStartElement())
            {
                if(xmlReader.name() == "day")
                {
                    while(xmlReader.readNextStartElement())
                    {
                        if(xmlReader.name() == "header")
                        {
                            tmpHeader = xmlReader.readElementText();
                            qDebug(qPrintable(tmpHeader));
                        }
                        xmlReader.readNextStartElement(); //
                        if(xmlReader.name() == "date")
                        {
                            tmpDate = xmlReader.readElementText();
                            qDebug(qPrintable(tmpDate));
                        }
                        xmlReader.readNextStartElement(); //
                        if(xmlReader.name() == "time")
                        {
                            tmpTime = xmlReader.readElementText();
                            qDebug(qPrintable(tmpTime));
                        }
                        xmlReader.readNextStartElement(); //
                        if(xmlReader.name() == "text")
                        {
                            tmpText = xmlReader.readElementText();
                            qDebug(qPrintable(tmpText));
                        }

                        createElement(tmpHeader, tmpDate, tmpTime, tmpText); // create entry with given data

                    }
                }
            }
        }
    }
    else {
        xmlReader.raiseError(QObject::tr("Incorrect file"));
    }


    qDebug()<< "loadList(): success";
    //emit dataChanged();
}

void EntryList::createList()
{
    // called if no profiles were found to create new profile
    QFile file(QCoreApplication::applicationDirPath() + "/profiles/user.xml"); //
       if (!file.open(QIODevice::ReadWrite | QIODevice::Text))
           return;

    qDebug()<< "createList(): success";
}
