#include "entry.h"
#include <QDebug>
//#include "pageday.h"

Entry::Entry(QObject* pobj) : QObject(pobj)
{
    //day = new PageDay();
    //header = day->getDate();
}


/*
void Entry::generateId()
{
    id.createUuid();
    qDebug()<<"new id: "<< id;
}
*/
void Entry::setHeader(QString header)
{
    if (m_header == header) {
        return;
    }

    m_header = header;
    emit headerChanged();
}

void Entry::setDate(QString date)
{
    if (m_date == date) {
        return;
    }

    m_date = date;
    emit dateChanged();
}

void Entry::setTime(QString time)
{
    if (m_time == time) {
        return;
    }

    m_time = time;
    emit timeChanged();
}

void Entry::setText(QString text)
{
    if (m_text == text) {
        return;
    }

    m_text = text;
    emit dateChanged();
}
