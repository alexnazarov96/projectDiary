import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import DayLogic 1.0

Rectangle {
    color: "#decd78"
    border.width: 1

    FontLoader {
        id: customFont
        name: "PlaneWalker"
        source: "qrc:///planewalker.ttf"
    }

    DayLogic {
        id: logic
    }

    Text {
        id: dateTimeArea
        x: 8
        y: 8
        width: 632
        height: 66

        color: "#fb1500"
        font.family: customFont.name
        verticalAlignment: Text.AlignVCenter
        font.pointSize: 20

        text: logic.date + "  " + logic.time
        wrapMode: Text.NoWrap
    }

    TextEdit {
        x: 8
        y: 80
        width: 624
        height: 379
        textFormat: Text.RichText
        wrapMode: Text.Wrap
        selectByMouse: true

        font.family: customFont.name
        font.pointSize: 14
        cursorVisible: true

        //cursorVisible: true

        text: logic.text
        onTextChanged: logic.text = text
    }
/*
    TextEdit {
        id: headerEditArea
        x: 8
        y: 80
        width: 624
        height: 31
        horizontalAlignment: Text.AlignHCenter
        font.family: "Courier"
        //cursorVisible: true
        font.pixelSize: 14

        text: logic.header
        onTextChanged: logic.header = text
    }
*/




}

/*
PageDayForm {
}
*/
