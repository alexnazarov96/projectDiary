#ifndef PAGEDAY_H
#define PAGEDAY_H
#include <QObject>

class PageDay : public QObject
{
Q_OBJECT

    Q_PROPERTY(QString date WRITE setDate READ getDate NOTIFY dateChanged)
    Q_PROPERTY(QString time WRITE setTime READ getTime NOTIFY timeChanged)
    Q_PROPERTY(QString text WRITE setText READ getText NOTIFY textChanged)

private:

    QString date;
    QString time;
    QString text;

public:
    PageDay(QObject* pobj = 0);

    void setDate(QString str);
    QString getDate() const { return date; }
    void setTime(QString str);
    QString getTime() const { return time; }
    void setText(QString str);
    QString getText() const { return text; }

    void initDate();
    void initTime();

signals:
    void dateChanged(QString date);
    void timeChanged(QString time);
    void textChanged(QString text);
};
#endif // PAGEDAY_H
#ifndef PAGEDAY_H
#define PAGEDAY_H

#endif // PAGEDAY_H
